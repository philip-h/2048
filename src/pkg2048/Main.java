package pkg2048;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;
import java.util.Scanner;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

/**
 *
 * @author philipjnr.habib
 */
public class Main extends KeyAdapter implements ActionListener
{
    private static final long serialVersionUID = 1L;
    private final JTextField[][] squares = new JTextField[4][4];
    private final JTextField[][] cache = new JTextField[4][4];
    private final JFrame frame;
    private boolean didWin = false;

    public Main()
    {
        frame = new JFrame();
        frame.setTitle("Philip's awesome 2048 game");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        frame.setVisible(true);
        frame.setFocusable(true);
    }

    private void init()
    {
        JPanel gridPane = new JPanel(new GridLayout(4, 4, 5, 5));

        setupGame(gridPane);

        frame.setContentPane(gridPane);
        frame.addKeyListener(this);
        setupMenuBar();
        createNumber();
    }

    private void setupGame(JPanel gridPane)
    {
        gridPane.setBackground(new Color(184, 148, 112));
        for (int i = 0; i < squares.length; i++)
        {
            for (int j = 0; j < squares[i].length; j++)
            {
                squares[i][j] = new JTextField();
                squares[i][j].setBackground(Color.lightGray);
                squares[i][j].setFont(new Font("Tahomr", Font.BOLD, 25));
                squares[i][j].setEditable(false);
                squares[i][j].setBorder(null);
                squares[i][j].setHorizontalAlignment(0);
                squares[i][j].setForeground(Color.darkGray);
                squares[i][j].setPreferredSize(new Dimension(100, 100));
                gridPane.add(squares[i][j]);
            }
        }
    }

    private void setupMenuBar()
    {
        JMenuBar menuBar = new JMenuBar();
        JMenu file = new JMenu("File");
        JMenuItem save = new JMenuItem("Save");
        JMenuItem load = new JMenuItem("Load");
        JMenuItem undo = new JMenuItem("Undo");

        save.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
        load.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
        undo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z, ActionEvent.CTRL_MASK));

        save.addActionListener(this);
        load.addActionListener(this);
        undo.addActionListener(this);

        file.add(save);
        file.add(load);
        file.add(undo);

        menuBar.add(file);
        frame.setJMenuBar(menuBar);
    }

    @Override
    public void keyPressed(KeyEvent e)
    {
        if (e.getKeyCode() == KeyEvent.VK_DOWN)
        {
            cacheGame();
            moveDown();
            afterMoveThings();
        } else if (e.getKeyCode() == KeyEvent.VK_RIGHT)
        {
            cacheGame();
            moveRight();
            afterMoveThings();
        } else if (e.getKeyCode() == KeyEvent.VK_UP)
        {
            cacheGame();
            moveUp();
            afterMoveThings();
        } else if (e.getKeyCode() == KeyEvent.VK_LEFT)
        {
            cacheGame();
            moveLeft();
            afterMoveThings();
        }
    }

    private void afterMoveThings()
    {
        createNumber();
        if (!didWin)
        {
            checkWin();
        }
        checkLose();
    }

    private void moveDown()
    {
        for (int i = squares.length - 1; i >= 0; i--)
        {
            for (int j = squares[i].length - 1; j >= 0; j--)
            {

                if (!squares[i][j].getText().equals(""))
                {
                    String text = squares[i][j].getText();
                    int tmp = i;
                    while (tmp < 3)
                    {
                        if (squares[tmp + 1][j].getText().equals(text))
                        {
                            setText(squares[tmp][j], 0);
                            int newText = Integer.parseInt(text);
                            newText *= 2;
                            setText(squares[tmp + 1][j], newText);
                        }
                        if (!squares[tmp + 1][j].getText().equals(""))
                        {
                            break;
                        }
                        setText(squares[tmp][j], 0);
                        setText(squares[tmp + 1][j], text);
                        tmp++;
                    }
                }
            }
        }
    }

    private void moveRight()
    {
        for (int i = squares.length - 1; i >= 0; i--)
        {
            for (int j = squares[i].length - 1; j >= 0; j--)
            {
                if (!squares[j][i].getText().equals(""))
                {
                    String text = squares[j][i].getText();
                    int tmp = i;
                    while (tmp < 3)
                    {
                        if (squares[j][tmp + 1].getText().equals(text))
                        {
                            setText(squares[j][tmp], 0);
                            int newText = Integer.parseInt(text);
                            newText *= 2;
                            setText(squares[j][tmp + 1], newText);
                        }
                        if (!squares[j][tmp + 1].getText().equals(""))
                        {
                            break;
                        }
                        setText(squares[j][tmp], 0);
                        setText(squares[j][tmp + 1], text);
                        tmp++;
                    }
                }
            }
        }
    }

    private void moveLeft()
    {
        for (int i = 0; i < squares.length; i++)
        {
            for (int j = 0; j < squares[i].length; j++)
            {
                if (!squares[j][i].getText().equals(""))
                {
                    String text = squares[j][i].getText();
                    int tmp = i;
                    while (tmp > 0)
                    {
                        if (squares[j][tmp - 1].getText().equals(text))
                        {
                            setText(squares[j][tmp], 0);
                            int newText = Integer.parseInt(text);
                            newText *= 2;
                            setText(squares[j][tmp - 1], newText);
                        }
                        if (!squares[j][tmp - 1].getText().equals(""))
                        {
                            break;
                        }
                        setText(squares[j][tmp], 0);
                        setText(squares[j][tmp - 1], text);
                        tmp--;
                    }
                }
            }
        }
    }

    private void moveUp()
    {
        for (int i = 0; i < squares.length; i++)
        {
            for (int j = 0; j < squares[i].length; j++)
            {

                if (!squares[i][j].getText().equals(""))
                {
                    String text = squares[i][j].getText();
                    int tmp = i;
                    while (tmp > 0)
                    {
                        if (squares[tmp - 1][j].getText().equals(text))
                        {
                            setText(squares[tmp][j], 0);
                            int newText = Integer.parseInt(text);
                            newText *= 2;
                            setText(squares[tmp - 1][j], newText);
                        }
                        if (!squares[tmp - 1][j].getText().equals(""))
                        {
                            break;
                        }
                        setText(squares[tmp][j], 0);
                        setText(squares[tmp - 1][j], text);
                        tmp--;
                    }
                }
            }
        }
    }

    private void createNumber()
    {
        Random ran = new Random();
        int ranX = ran.nextInt(4);
        int ranY = ran.nextInt(4);
        while (!squares[ranX][ranY].getText().equals(""))
        {
            ranX = ran.nextInt(4);
            ranY = ran.nextInt(4);
        }
        JTextField newButton = squares[ranX][ranY];
        int randomNumber = ran.nextInt(8);
        if (randomNumber < 7)
        {
            setText(newButton, 2);
        } else
        {
            setText(newButton, 4);
        }
    }

    private void checkWin()
    {
        for (int i = 0; i < squares.length; i++)
        {
            for (int j = 0; j < squares[i].length; j++)
            {
                if (squares[i][j].getText().equals("2048"))
                {
                    int choice = JOptionPane.showConfirmDialog(null, "You Win!! Do you want to keep going?", "Winner", JOptionPane.OK_CANCEL_OPTION);
                    if (choice == JOptionPane.CANCEL_OPTION)
                    {
                        System.exit(0);
                    } else
                    {
                        didWin = true;
                    }
                }
            }
        }
    }

    private void checkLose()
    {
        for (int i = 0; i < squares.length; i++)
        {
            for (int j = 0; j < squares[i].length; j++)
            {
                if (squares[i][j].getText().equals(""))
                {
                    return;
                }
            }
        }
        int choice = JOptionPane.showConfirmDialog(
                null, "You are a loser, Play again?", "LOSER", JOptionPane.OK_CANCEL_OPTION);
        if (choice == JOptionPane.OK_OPTION)
        {
            restart();
        } else
        {
            System.exit(0);
        }
    }

    private void setText(JTextField button, int number)
    {
        if (number == 0)
        {
            button.setBackground(Color.lightGray);
            button.setText("");
        }
        if (number == 2)
        {
            button.setBackground(Color.white);
            button.setText("2");
        } else if (number == 4)
        {
            button.setBackground(new Color(255, 204, 102));
            button.setText("4");
        } else if (number == 8)
        {
            button.setBackground(new Color(255, 188, 89));
            button.setText("8");
        } else if (number == 16)
        {
            button.setBackground(new Color(255, 153, 0));
            button.setText("16");
        } else if (number == 32)
        {
            button.setBackground(new Color(255, 51, 0));
            button.setText("32");
        } else if (number == 64)
        {
            button.setBackground(Color.red);
            button.setText("64");
        } else if (number == 128)
        {
            button.setBackground(new Color(255, 255, 148));
            button.setText("128");
        } else if (number == 256)
        {
            button.setBackground(new Color(255, 255, 148));
            button.setText("256");
        } else if (number == 512)
        {
            button.setBackground(new Color(230, 230, 0));
            button.setText("512");
        } else if (number == 1024)
        {
            button.setBackground(new Color(230, 230, 0));
            button.setText("1024");
        } else if (number == 2048)
        {
            button.setBackground(new Color(255, 255, 0));
            button.setText("2048");
        } else if (number == 4096)
        {
            button.setBackground(new Color(153, 255, 51));
            button.setText("4096");
        } else if (number == 8192)
        {
            button.setBackground(Color.blue);
            button.setText("8192");
        } else if (number == 16384)
        {
            button.setBackground(Color.magenta);
            button.setText("16384");
        } else if (number == 32768)
        {
            button.setBackground(Color.pink);
            button.setText("32768");
        } else if (number == 65536)
        {
            button.setBackground(null);
            button.setText("65536");
        }

    }

    private void setText(JTextField button, String number)
    {
        setText(button, Integer.parseInt(number));
    }

    public void restart()
    {
        frame.dispose();
        new Main();
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        JMenuItem item = (JMenuItem) e.getSource();
        switch (item.getText())
        {
            case "Save":
                saveGame();
                break;
            case "Load":
                loadGame();
                break;
            case "Undo":
                undoMove();
                break;
        }
    }

    private void saveGame()
    {
        PrintWriter output = null;
        try
        {
            output = new PrintWriter(new FileWriter("savedGame.save"));
        } catch (IOException ex)
        {
            JOptionPane.showMessageDialog(null, "Unable to create new file writer: " + ex.getMessage());
        }
        if (output != null)
        {
            for (int i = 0; i < squares.length; i++)
            {
                for (int j = 0; j < squares[i].length; j++)
                {
                    if (squares[i][j].getText().equals(""))
                    {
                        output.print("@ ");
                    }
                    output.print(squares[i][j].getText() + " ");
                }
                output.println();
            }
            output.close();
            JOptionPane.showMessageDialog(null, "Success", "The game has been saved!", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    private void loadGame()
    {
        Scanner input = null;
        try
        {
            input = new Scanner(new FileReader("savedGame.save"));
        } catch (FileNotFoundException ex)
        {
            JOptionPane.showMessageDialog(null, "Unable to laod save file. Please make sure you save a game before loading it");
        }
        if (input != null)
        {
            for (int i = 0; i < squares.length; i++)
            {
                for (int j = 0; j < squares[i].length; j++)
                {
                    String num = input.next();
                    if (num.equals("@"))
                    {
                        setText(squares[i][j], 0);
                        squares[i][j].setText("");
                    } else
                    {
                        setText(squares[i][j], num);
                    }
                }
            }
        }
    }

    private void undoMove()
    {
        if (cache[0][0] != null)
        {
            for (int i = 0; i < cache.length; i++)
            {
                for (int j = 0; j < cache[i].length; j++)
                {
                    if (cache[i][j].getText().equals(""))
                    {
                        setText(squares[i][j], 0);
                    } else
                    {
                        setText(squares[i][j], cache[i][j].getText());
                    }
                }
            }
        }
    }

    private void cacheGame()
    {
        for (int i = 0; i < squares.length; i++)
        {
            for (int j = 0; j < squares[i].length; j++)
            {
                cache[i][j] = new JTextField(squares[i][j].getText());
            }
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        new Main();
    }
}
